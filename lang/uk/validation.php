<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted' => 'Значення :attribute повинно бути прийнято.',
    'active_url' => 'Значення :attribute не є дійсним URL.',
    'after' => 'Значення :attribute має бути датою після :date.',
    'after_or_equal' => 'Значення :attribute має бути датою після або дорівнювати :date.',
    'alpha' => 'Значення :attribute має містити лише літери.',
    'alpha_dash' => 'Значення :attribute має містити лише літери, цифри, тире та підкреслення.',
    'alpha_num' => 'Значення має містити лише літери та цифри.',
    'array' => 'Значення має бути масивом.',
    'before' => 'Значення має бути датою перед :date.',
    'before_or_equal' => 'Значення має бути датою, що передує або дорівнює :date.',
    'between' => [
        'numeric' => 'Значення має бути між :min та :max.',
        'file' => 'Значення має бути від :min до :max кілобайт.',
        'string' => 'Значення має бути від :min до :max символів.',
        'array' => 'Значення має містити між :min та :max елементами.',
    ],
    'boolean' => 'Значення має бути true або false!',
    'confirmed' => 'Значення підтвердження не відповідає.',
    'current_password' => 'Пароль невірний.',
    'date' => 'Значення не є датою.',
    'date_equals' => 'Значення має бути датою, що дорівнює :date.',
    'date_format' => 'Значення не відповідає формату :format.',
    'different' => 'Значення та :other повинні бути різними.',
    'digits' => 'Значення має бути :digits digits.',
    'digits_between' => 'Значення має бути між :min та :max цифрами.',
    'dimensions' => 'Значення має недійсні розміри зображення.',
    'distinct' => 'Поле Значення має повторюване значення.',
    'email' => 'Значення має бути дійсною електронною адресою.',
    'ends_with' => 'Значення має закінчуватися одним із наступного: :values.',
    'exists' => 'Вибраний :attribute недійсний.',
    'file' => 'Значення має бути файлом.',
    'filled' => 'Поле значення повинно мати значення.',
    'gt' => [
        'numeric' => 'Значення має бути більше ніж :value.',
        'file' => 'Значення має бути більше ніж :value kilobytes.',
        'string' => 'Значення має бути більше ніж :value символів.',
        'array' => 'Значення повинно мати більше ніж :value items.',
    ],
    'gte' => [
        'numeric' => 'Значення має бути більше або дорівнювати :value.',
        'file' => 'Значення має бути більше або дорівнювати :value kilobytes.',
        'string' => 'Значення має бути більше або дорівнювати символам :value.',
        'array' => 'Значення повинно мати :value items або більше.',
    ],
    'image' => 'Значення має бути зображенням.',
    'in' => 'Вибраний :attribute недійсний.',
    'in_array' => 'Поле значення не існує в :other.',
    'integer' => 'Значення має бути цілим числом.',
    'ip' => 'Значення має бути дійсною IP-адресою.',
    'ipv4' => 'Значення має бути дійсною адресою IPv4.',
    'ipv6' => 'Значення має бути дійсною адресою IPv6.',
    'json' => 'Значення має бути дійсним рядком JSON.',
    'lt' => [
        'numeric' => 'Значення має бути менше :value.',
        'file' => 'Значення має бути менше :value кілобайт.',
        'string' => 'Значення має бути менше :value символів.',
        'array' => 'Значення повинно мати менше ніж :value items.',
    ],
    'lte' => [
        'numeric' => 'Значення має бути менше або дорівнювати :value.',
        'file' => 'Значення має бути менше або дорівнювати :value kilobytes.',
        'string' => 'Значення має бути менше або дорівнювати символам :value.',
        'array' => 'Значення не повинно містити більше ніж :value items.',
    ],
    'max' => [
        'numeric' => 'Значення не повинно перевищувати :max.',
        'file' => 'Значення не повинно перевищувати :max кілобайт.',
        'string' => 'Значення не повинно перевищувати :max символів.',
        'array' => 'Значення не повинно містити більше ніж :max елементів.',
    ],
    'mimes' => 'Значення має бути файлом типу: :values.',
    'mimetypes' => 'Значення має бути файлом типу: :values.',
    'min' => [
        'numeric' => 'Значення має бути не менше :min.',
        'file' => 'Значення має бути не менше :min кілобайт.',
        'string' => 'Значення має містити принаймні :min символів.',
        'array' => 'Значення повинно мати принаймні :min елементів.',
    ],
    'multiple_of' => 'Значення має бути кратним :value.',
    'not_in' => 'Вибраний :attribute недійсний.',
    'not_regex' => 'Недійсний формат позначення.',
    'numeric' => 'Значення має бути числом!',
    'password' => 'Пароль невірний.',
    'present' => 'Поле значення повинно бути присутнім.',
    'regex' => 'Формат значення недійсний.',
    'required' => 'Значення є обов\'язковим!',
     'required_if' => 'Поле значення є обов\'язковим, якщо :other є :value.',
     'required_unless' => 'Поле значення є обов\'язковим, якщо :other не міститься в :values.',
     'required_with' => 'Поле значення є обов\'язковим, якщо присутній :values.',
     'required_with_all' => 'Поле значення є обов\'язковим, якщо присутні :values.',
     'required_without' => 'Поле "Значення" є обов\'язковим, якщо :values відсутній.',
     'required_without_all' => 'Поле Значення є обов\'язковим, якщо немає жодного з :values.',
     'prohibited' => 'Значення поля заборонено.',
     'prohibited_if' => 'Поле значення заборонено, якщо :other дорівнює :value.',
     'prohibited_unless' => 'Значення поля заборонено, якщо :other не міститься в :values.',
     'same' => 'Значення та :other повинні збігатися.',
    'size' => [
        'numeric' => 'Значення має бути :size.',
        'file' => 'Значення має бути :size кілобайт.',
        'string' => 'Значення має бути :size символів.',
        'array' => 'Значення має містити :size items.',
    ],
    'starts_with' => 'Значення має починатися з одного з наступного: :values.',
    'string' => 'Значення має бути рядком.',
    'timezone' => 'Значення має бути дійсним часовим поясом.',
    'unique' => 'Значення вже прийнято.',
    'uploaded' => 'Значення не вдалося завантажити.',
    'url' => 'Значення має бути дійсним URL.',
    'uuid' => 'Значення має бути дійсним UUID.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap our attribute placeholder
    | with something more reader friendly such as "E-Mail Address" instead
    | of "email". This simply helps us make our message more expressive.
    |
    */

    'attributes' => [],

    'slug' => [
        'required' => 'Необхідно вказати slug',
        'unique' => 'Такий slug вже існує',
    ],
    'price' => [
        'required' => 'Необхідно вказати ціну',
    ],
    'quantity' => [
        'required' => 'Необхідно вказати кількість',
    ],
    'name' => [
        'required' => 'Необхідно вказати назву',
    ],
    'consist' => [
        'required' => 'Необхідно вказати склад',
    ],
    'description' => [
        'required' => 'Необхідно вказати опис',
    ],
    'seo_title' => 'Необхідно вказати seo_title',
    'seo_description' => 'Необхідно вказати seo_description',
    'filters' => 'Необхідно обрати мінімум один фільтр',
    'categories' => 'Необхідно обрати мінімум одну категорію',
    'manufacturer' => 'Необхідно обрати виробника',

];
