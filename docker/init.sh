docker exec -ti skelar-php sh -c "composer update"
docker exec -ti skelar-php sh -c "php artisan key:generate"
docker exec -ti skelar-php sh -c "php artisan migrate:refresh --seed"
