<?php

namespace App\Services\Crud;

class CrudButton
{
    const BUTTON_SHOW = 'show-button';
    const BUTTON_EDIT = 'edit-button';
    const BUTTON_DELETE = 'delete-button';
    const BUTTON_LINK = 'link-button';
    const BUTTON_CLICK = 'click-button';

    public string $name;
    public string $label;
    public string $type;
    public string $link;
    public string $linkClass;
    public string $iconClass;

    public function setName(string $name)
    {
        $this->name = $name;
    }

    public function setLabel(string $label)
    {
        $this->label = $label;
    }

    public function setType(string $type)
    {
        $this->type = $type;
    }

    public function setLink(string $link)
    {
        $this->link = $link;
    }

    public function setLinkClass(string $linkClass)
    {
        $this->linkClass = $linkClass;
    }

    public function setIconClass(string $iconClass)
    {
        $this->iconClass = $iconClass;
    }
}

