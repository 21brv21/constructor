<?php

namespace App\Services\Crud;

use Illuminate\Support\Str;

class CrudColumn
{
    //DEFAULT
    const COLUMN_TEXT = 'text-column';
    const COLUMN_MULTIPLE = 'multiple-column';
    const COLUMN_BOOLEAN = 'boolean-column';
    const COLUMN_COLOR = 'color-column';
    const COLUMN_PHONE = 'phone-column';
    const COLUMN_NUMBER = 'number-column';
    const COLUMN_IMAGE = 'image-column';
    const COLUMN_COPY = 'copy-column';

    //CUSTOM
    const COLUMN_STATUS = 'status-column';
    const COLUMN_STICKER = 'sticker-column';

    public string $name;
    public string $label;
    public string $type;
    public int $length;
    public string $classes;
    public bool $sort;

    public function __construct()
    {
        $this->type = \App\Services\Crud\CrudColumn::COLUMN_TEXT;
        $this->sort = false;
        $this->length = 50;//довжина обмеження стовбця
    }

    public function setName(string $name)
    {
        $this->name = Str::snake($name);
    }

    public function setLabel(string $label)
    {
        $this->label = $label;
    }

    public function setType(string $type)
    {
        $this->type = $type;
    }

    public function setSort(bool $sort)
    {
        $this->sort = $sort;
    }

    public function setLength(int $length)
    {
        $this->length = $length;
    }

    public function setClasses(string $classes)
    {
        $this->classes = $classes;
    }
}

