<?php

namespace App\Services\Crud;

trait Operations
{
    public $operation = '';
    public $allowShow = false;
    public $allowCreate = true;
    public $allowUpdate = true;
    public $allowDelete = true;

    public function setOperation(string $operation_name)
    {
        $this->operation = $operation_name;
    }

    public function getOperation(): string
    {
        return $this->operation;
    }

    public function getOperationName(): string
    {
        return trans('crud.'.$this->operation);
    }

    public function setAllowShow(bool $allow)
    {
        $this->allowShow = $allow;
    }

    public function getAllowShow(): bool
    {
        return $this->allowShow;
    }

    public function setAllowCreat(bool $allow)
    {
        $this->allowCreate = $allow;
    }

    public function getAllowCreat(): bool
    {
        return $this->allowCreate;
    }

    public function setAllowUpdate(bool $allow)
    {
        $this->allowUpdate = $allow;
    }

    public function getAllowUpdate(): bool
    {
        return $this->allowUpdate;
    }

    public function setAllowDelete(bool $allow)
    {
        $this->allowDelete = $allow;
    }

    public function getAllowDelete(): bool
    {
        return $this->allowDelete;
    }


    public function checkAllowCreate()
    {
        if (!$this->allowCreate) {
            throw new \Exception(trans('crud.notCreate'), 500);
        }
    }

    public function checkAllowUpdate()
    {
        if (!$this->allowUpdate) {
            throw new \Exception(trans('crud.notUpdate'), 500);
        }
    }

    public function checkAllowDelete()
    {
        if (!$this->allowDelete) {
            throw new \Exception(trans('crud.notDelete'), 500);
        }
    }
}
