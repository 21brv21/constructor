<?php

namespace App\Services\Crud;

trait Filters
{
    public $filters = [];

    public function addFilter(CrudFilter $filter)
    {
        $this->filters[$filter->name] = $filter;
    }

    public function getFilters(): array
    {
        return $this->filters;
    }

    public function createFilter(array $options, $values = false, $logic = false)
    {
        $this->checkFiltersOptions($options);

        $filter = new CrudFilter();
        $filter->setName($options['name']);
        $filter->setPlaceholder($options['placeholder']);
        $filter->setLogic($logic);

        if (isset($options['type'])) $filter->setType($options['type']);
        if (isset($options['class'])) $filter->setClass($options['class']);
        if (isset($options['keyReduce'])) $filter->setKeyReduce($options['keyReduce']);
        if (is_callable($values)) $filter->setValues($values());

        $this->addFilter($filter);
    }


    public function checkFiltersOptions(array $options)
    {
        if (!isset($options['name'])) {
            abort(500, trans('crud.errorsFilters.name'));
        }
        if (!isset($options['placeholder'])) {
            abort(500, trans('crud.errorsFilters.placeholder'));
        }
    }
}
