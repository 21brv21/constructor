<?php

namespace App\Services\Crud;

trait Fields
{
    public array $fields = [];

    public function getFields(): array
    {
        return $this->fields;
    }

    public function addField(CrudField $field)
    {
        $this->fields[$field->name] = $field;
    }

    public function createField(array $options)
    {
        $this->checkFieldOptions($options);

        $field = new CrudField();
        $field->setName($options['name']);
        $field->setLabel($options['label']);


        if (isset($options['placeholder'])) {
            $field->setPlaceholder($options['placeholder']);
        }else{
            $field->setPlaceholder($options['label']);
        }

        if (isset($options['type'])) $field->setType($options['type']);
        if (isset($options['required'])) $field->setRequired($options['required']);
        if (isset($options['class'])) $field->setClass($options['class']);
        if (isset($options['defaultValue'])) $field->setDefaultValue($options['defaultValue']);
        if (isset($options['values'])) $field->setValues($options['values']);
        if (isset($options['options'])) foreach ($options['options'] as $option => $value) $field->addOptions($option, $value);

        $this->addField($field);
    }

    public function checkFieldOptions(array $options)
    {
        if (!isset($options['name'])) {
            abort(500, 'Переконайтеся, що всі ваші поля мають name');
        }

        if (!isset($options['label'])) {
            abort(500, 'Переконайтеся, що всі ваші поля мають label');
        }
    }
}
