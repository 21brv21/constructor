<?php

namespace App\Services\Crud;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Nette\Schema\ValidationException;
use stdClass;

class CrudPanel
{
    use Fields;
    use Filters;
    use Columns;
    use Buttons;
    use Operations;

    public Builder $query;
    public object $entry;
    public object $model;
    public array $relations = [];
    public array $relationsMany = [];
    public array $relationsOnlyForView = [];
    public array $relationsManyOnlyForView = [];
    public string $route;
    public string $view = 'Crud';
    public string $viewIndex = 'Crud/Index';
    public string $viewCreate = 'Crud/Create';
    public string $viewEdit = 'Crud/Edit';
    public string $viewShow = 'Crud/Show';
    public string $validateRequest = Request::class;
    public array $validateRules;
    public array $validateMessages;

    public string $entityName;
    public string $entityNamePlural;
    public string $entityNameAdd;

    public function __construct()
    {

    }


    //----------------------------Model----------------------------
    public function setModel($modelNamespace)
    {
        if (!class_exists($modelNamespace)) {
            throw new ValidationException('The model does not exist.', 500);
        }

        $this->model = new $modelNamespace();
        $this->query = $this->model->select('*');
        $this->entry = new $modelNamespace();
    }

    public function getModel(): object
    {
        return $this->model;
    }

    public function getModelEdit(): object
    {
        return $this->getModel();
    }

    public function getKeyName(): string
    {
        return $this->entry->getModel()->getKeyName() ? $this->entry->getModel()->getKeyName() : 'id';
    }

    public function getQuery():Builder
    {
        return $this->query;
    }

    public function getQuerySearch():Builder
    {
        return $this->query;
    }



    //----------------------------Relations----------------------------

    public function addRelation(string $relation)
    {
        $this->relations[] = $relation;
        $this->model = $this->model->with($relation);
        $this->entry = $this->entry->with($relation);
        $this->query = $this->query->with($relation);
    }

    public function addRelationMany(string $relation)
    {
        $this->relationsMany[] = $relation;
        $this->model = $this->model->with($relation);
        $this->entry = $this->entry->with($relation);
        $this->query = $this->query->with($relation);
    }

    public function addRelationOnlyForView(string $relation)
    {
        $this->relationsOnlyForView[] = $relation;
        $this->model = $this->model->with($relation);
    }

    public function addRelationManyOnlyForView(string $relation)
    {
        $this->relationsManyOnlyForView[] = $relation;
        $this->model = $this->model->with($relation);
    }

    public function getRelations(): array
    {
        return $this->relations;
    }

    public function getRelationsMany(): array
    {
        return $this->relationsMany;
    }

    public function getRelationsOnlyForView(): array
    {
        return $this->relationsOnlyForView;
    }

    public function getRelationsManyOnlyForView(): array
    {
        return $this->relationsManyOnlyForView;
    }

    //----------------------------ValidateRequest----------------------------
    public function setValidateRequest(string $validateRequest)
    {
        if (!class_exists($validateRequest)) {
            throw new ValidationException('The validate Request does not exist.');
        }

        $validateRequest = new $validateRequest();
        $this->validateRequest = $validateRequest;
        $this->validateRules = $validateRequest->rules();
        $this->validateMessages = $validateRequest->messages();
    }


    public function getValidateRules()
    {
        return $this->validateRules;
    }

    public function getValidateMessages()
    {
        return $this->validateMessages;
    }


    //----------------------------Route----------------------------

    public function setRoute(string $route)
    {
        $this->route = $route;
    }

    public function getRoute(): string
    {
        return $this->route;
    }

    public function getCreateRoute(): string
    {
        return $this->route . 'create';
    }

    public function getEditRoute(): string
    {
        $key = $this->getKeyName();
        return $this->route . $this->entry->$key . '/edit';
    }


    //----------------------------View----------------------------

    public function setViewIndex(string $view)
    {
        $this->viewIndex = $view;
    }

    public function setViewCreate(string $view)
    {
        $this->viewCreate = $view;
    }

    public function setViewEdit(string $view)
    {
        $this->viewEdit = $view;
    }


    public function setViewShow(string $view)
    {
        $this->viewShow = $view;
    }

    public function getViewIndex(): string
    {
        return $this->viewIndex;
    }

    public function getViewCreate(): string
    {
        return $this->viewCreate;
    }

    public function getViewEdit(): string
    {
        return $this->viewEdit;
    }

    public function getViewShow(): string
    {
        return $this->viewShow;
    }


    //----------------------------NAMES----------------------------

    public function setEntityNames(string $entityName, string $entityNamePlural, string $entityNameAdd = null)
    {
        $this->entityName = $entityName;
        $this->entityNamePlural = $entityNamePlural;
        $this->entityNameAdd = $entityNameAdd ?? $entityName;
    }

    public function getEntityName(): string
    {
        return $this->entityName;
    }

    public function getEntityNamePlural(): string
    {
        return $this->entityNamePlural;
    }

    public function getEntityNameAdd(): string
    {
        return $this->entityNameAdd;
    }

    //----------------------------Add columns, fields,filters----------------------------

    public function getOptions()
    {
        $options = new stdClass();
        $options->route = $this->getRoute();
        $options->operation = $this->getOperation();
        $options->operationName = $this->getOperationName();
        $options->allowShow = $this->getAllowShow();
        $options->allowCreate = $this->getAllowCreat();
        $options->allowUpdate = $this->getAllowUpdate();
        $options->allowDelete = $this->getAllowDelete();
        $options->entityName = $this->getEntityName();
        $options->entityNamePlural = $this->getEntityNamePlural();
        $options->entityNameAdd = $this->getEntityNameAdd();
        $options->entityKeyName = $this->getKeyName();
        return $options;
    }
}
