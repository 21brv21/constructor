<?php

namespace App\Services\Crud;

class CrudFilter
{
    const FILTER_INPUT = 'input-filter';
    const FILTER_SELECT2 = 'select2-filter';
    const FILTER_SELECT2_KEY = 'select2-key-filter';
    const FILTER_SELECT2_BY_KEY = 'select2-by-key-filter';
    const FILTER_SEARCH_API = 'search-api-filter';

    public $logic;
    public string $name;
    public string $placeholder;
    public array $values = [];
    public string $classes;
    public string $type;
    public string $class;
    public string $keyReduce;


    public function __construct()
    {
        $this->type = CrudFilter::FILTER_SELECT2;
        $this->class = 'col-md-3';
        $this->keyReduce = 'id';
    }


    public function setName(string $name)
    {
        $this->name = $name;
    }

    public function setPlaceholder(string $placeholder)
    {
        $this->placeholder = $placeholder;
    }

    public function setValues(array $values)
    {
        $this->values = $values;
    }

    public function setLogic($logic)
    {
        $this->logic = $logic;
    }

    public function setClasses(string $classes)
    {
        $this->classes = $classes;
    }

    public function setType(string $type)
    {
        $this->type = $type;
    }

    public function setClass(string $class)
    {
        $this->class = $class;
    }

    public function setKeyReduce(string $keyReduce)
    {
        $this->keyReduce = $keyReduce;
    }
}
