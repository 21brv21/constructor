<?php

namespace App\Services\Crud\PanelTraits;


trait HelpersOperation
{
    public function filteredDataRequest(array $data):array
    {
        return $data;
    }

    public function syncRelations(array $data)
    {
        $relationsMany = array_merge($this->crud->getRelationsMany(), $this->crud->getRelationsManyOnlyForView());
        foreach ($relationsMany as $relation) {
            if (isset($data[$relation])) {
                $this->crud->entry->$relation()->sync($data[$relation]);
            }
        }
    }

    public function getFiles(): array
    {
        $files = [];
        if (isset($this->crud->entry->image)) {
            $files[] = $this->crud->entry->getRawOriginal('image');
        }
        return $files;
    }

    public function redirectAfterSave():string
    {
        return $this->crud->getRoute();
    }

}
