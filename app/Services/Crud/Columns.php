<?php

namespace App\Services\Crud;

use Illuminate\Support\Str;

trait Columns
{
    public array $columns = [];

    public function addColumn(CrudColumn $column)
    {
        $this->columns[$column->name] = $column;
    }

    public function getColumns():array
    {
        return $this->columns;
    }

    public function createColumn(array $options)
    {
        $this->checkColumnsOptions($options);

        $column = new CrudColumn();
        $column->setName($options['name']);
        $column->setLabel($options['label']);

        if (isset($options['type'])) $column->setType($options['type']);
        if (isset($options['sort'])) $column->setSort($options['sort']);
        if (isset($options['length'])) $column->setLength($options['length']);
        if (isset($options['classes'])) $column->setClasses($options['classes']);

        $this->addColumn($column);
    }

    public function checkColumnsOptions(array $options)
    {
        if (!isset($options['name'])) {
            abort(500, trans('crud.errorsColumns.name'));
        }
        if (!isset($options['label'])) {
            abort(500, trans('crud.errorsColumns.label'));
        }
    }
}
