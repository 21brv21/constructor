<?php

namespace App\Services\Crud;


class CrudField
{
    const INPUT_TEXT = 'text-input';
    const INPUT_TEXTAREA = 'textarea-input';
    const INPUT_IMAGE = 'image-input';
    const INPUT_PHONE = 'phone-input';
    const INPUT_EDITOR = 'editor-input';
    const INPUT_JSON = 'json-input';
    const INPUT_SELECT2_MULTIPLE = 'select2-multiple-input';
    const INPUT_SLUG = 'slug-input';
    const INPUT_SELECT2 = 'select2-input';
    const INPUT_SELECT2_BY_KEY = 'select2-by-key-input';
    const INPUT_ARRAY_SELECT2 = 'select2-array-input';
    const INPUT_COLOR = 'color-input';
    const INPUT_DATE = 'date-input';
    const INPUT_BOOLEAN = 'boolean-input';
    const INPUT_RADIO= 'radio-input';
    const INPUT_RADIO_COLOR= 'radio-color-input';
    const INPUT_FILE = 'file-input';
    const INPUT_HAS_MANY = 'has-many-input';
    const INPUT_HAS_MANY_TEXT = 'has-many-text-input';
    const INPUT_SELECT = 'select-input';
    const INPUT_NUMBER = 'number-input';
    const INPUT_FLOAT = 'float-input';

    const INPUT_PASSWORD = 'password-input';
    const INPUT_HIDDEN = 'hidden-input';

    //имя поля
    public string $name;

    //тип поля
    public string $type;

    //label поля
    public string $label;

    //placeholder поля
    public string $placeholder;

    //required ли
    public bool $required;

    //список
    public array $values;

    //опції
    public object $options;

    //defaultValue
    public string $defaultValue;

    //class
    public string $class;

    public function __construct()
    {
        $this->options = new \stdClass();
        $this->type = self::INPUT_TEXT;
        $this->class = 'col-md-3';
    }

    public function setName(string $name)
    {
        $this->name = $name;
    }

    public function setType(string $type)
    {
        $this->type = $type;
    }

    public function setLabel(string $label)
    {
        $this->label = $label;
    }

    public function setRequired(bool $required)
    {
        $this->required = $required;
    }

    public function setDefaultValue(string $defaultValue)
    {
        $this->defaultValue = $defaultValue;
    }

    public function setClass(string $class)
    {
        $this->class = $class;
    }

    public function setPlaceholder(string $placeholder)
    {
        $this->placeholder = $placeholder;
    }

    public function setValues(array $values)
    {
        $this->values = $values;
    }

    public function addOptions($name, $value)
    {
        //reduceKey
        //reduceLabel for select 2
        $this->options->$name = $value;
    }
}
