<?php

namespace App\Http\Controllers;

use App\Http\Requests\CrudManufacturerRequest;
use App\Models\Manufacturer;
use App\Services\Crud\CrudFilter;

class ManufacturerCrudController extends CrudController
{
    public function setup()
    {
        $this->crud->setModel(Manufacturer::class);
        $this->crud->setValidateRequest(CrudManufacturerRequest::class);
        $this->crud->setRoute('/manufacturers/');
        $this->crud->setEntityNames('виробник', 'Виробники', 'виробника');

        $this->setupColumns();
        $this->setupFields();
        $this->setupFilters();
    }

    private function setupColumns()
    {
        $this->crud->createColumn([
            'name' => 'name',
            'label' => 'Назва',
            'sort' => true,
        ]);
    }

    private function setupFields()
    {
        $this->crud->createField([
            'name' => 'name',
            'class' => 'col-md-12',
            'label' => 'Назва',
            'required' => true,
        ]);
    }

    private function setupFilters()
    {
        $this->crud->createFilter([
            'name' => 'name',
            'type' => CrudFilter::FILTER_SELECT2_KEY,
            'placeholder' => 'Батьківська категорія',
        ], function () {
            return Manufacturer::all()->toArray();
        }, function ($value) {
            $this->crud->query = $this->crud->query->where('name', 'LIKE', '%'.$value.'%');
        });
    }
}
