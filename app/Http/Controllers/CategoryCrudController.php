<?php

namespace App\Http\Controllers;

use App\Http\Requests\CrudCategoryRequest;
use App\Models\Category;
use App\Services\Crud\CrudField;
use App\Services\Crud\CrudFilter;

class CategoryCrudController extends CrudController
{
    public function setup()
    {
        $this->crud->setModel(Category::class);
        $this->crud->setValidateRequest(CrudCategoryRequest::class);
        $this->crud->setRoute('/categories/');
        $this->crud->setEntityNames('категорія', 'Категорії', 'категорію');
        $this->crud->addRelation('parent');

        $this->setupColumns();
        $this->setupFields();
        $this->setupFilters();
    }

    private function setupColumns()
    {
        $this->crud->createColumn([
            'name' => 'name',
            'label' => 'Назва',
            'sort' => true,
        ]);

        $this->crud->createColumn([
            'name' => 'parent.name',
            'label' => 'Батьківська категорія',
        ]);
    }

    private function setupFields()
    {
        $this->crud->createField([
            'name' => 'name',
            'class' => 'col-md-6',
            'label' => 'Назва',
            'required' => true,
        ]);

        $this->crud->createField([
            'name' => 'parent_id',
            'class' => 'col-md-6',
            'label' => 'Батьківська категорія',
            'type' => CrudField::INPUT_SELECT2,
            'values' => Category::all()->toArray(),
            'required' => true,
        ]);
    }

    private function setupFilters()
    {
        $this->crud->createFilter([
            'name' => 'name',
            'class' => 'col-md-3',
            'type' => CrudFilter::FILTER_INPUT,
            'placeholder' => 'Назва',
        ], function () {
            return [];
        }, function ($value) {
            $this->crud->query = $this->crud->query->where('name', 'LIKE', '%'.$value.'%');
        });

        $this->crud->createFilter([
            'name' => 'parent_id',
            'type' => CrudFilter::FILTER_SELECT2_KEY,
            'placeholder' => 'Батьківська категорія',
        ], function () {
            return Category::all()->toArray();
        }, function ($value) {
            $this->crud->query = $this->crud->query->where('parent_id', $value);
        });
    }
}
