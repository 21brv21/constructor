<?php

namespace App\Http\Controllers;

use App\Services\Crud\CrudPanel;
use App\Services\Crud\PanelTraits\CreateOperation;
use App\Services\Crud\PanelTraits\DeleteOperation;
use App\Services\Crud\PanelTraits\HelpersOperation;
use App\Services\Crud\PanelTraits\ListOperation;
use App\Services\Crud\PanelTraits\ShowOperation;
use App\Services\Crud\PanelTraits\UpdateOperation;

abstract class CrudController extends Controller
{
    use ListOperation;
    use ShowOperation;
    use CreateOperation;
    use UpdateOperation;
    use DeleteOperation;
    use HelpersOperation;

    public $crud;

    public function __construct()
    {
        if (!$this->crud) {
            $this->crud = new CrudPanel();
            $this->middleware(function ($request, $next) {
                $this->setup();
                return $next($request);
            });
        }
    }

    abstract protected function setup();
}
