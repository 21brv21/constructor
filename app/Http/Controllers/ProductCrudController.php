<?php

namespace App\Http\Controllers;

use App\Http\Requests\CrudProductRequest;
use App\Models\Category;
use App\Models\Product;
use App\Services\Crud\CrudColumn;
use App\Services\Crud\CrudField;
use App\Services\Crud\CrudFilter;

class ProductCrudController extends CrudController
{
    public function setup()
    {
        $this->crud->setModel(Product::class);
        $this->crud->setValidateRequest(CrudProductRequest::class);
        $this->crud->setRoute('/products/');
        $this->crud->setEntityNames('товар', 'Товари');
        $this->crud->addRelation('manufacturer');
        $this->crud->addRelationMany('categories');

        $this->setupColumns();
        $this->setupFields();
        $this->setupFilters();
    }

    private function setupColumns()
    {
        $this->crud->createColumn([
            'name' => 'id',
            'label' => '#',
            'sort' => true,
        ]);

        $this->crud->createColumn([
            'name' => 'name',
            'label' => 'Назва',
            'sort' => true,
        ]);

        $this->crud->createColumn([
            'name' => 'categories.*.name',
            'type' => CrudColumn::COLUMN_MULTIPLE,
            'label' => 'Категорії',
            'length' => 17,
        ]);

        $this->crud->createColumn([
            'name' => 'price',
            'label' => '$',
            'type' => CrudColumn::COLUMN_NUMBER,
            'sort' => true,
        ]);

        $this->crud->createColumn([
            'name' => 'quantity',
            'label' => 'К-сть',
            'type' => CrudColumn::COLUMN_NUMBER,
            'sort' => true,
        ]);

        $this->crud->createColumn([
            'name' => 'status',
            'label' => 'Статус',
            'type' => CrudColumn::COLUMN_STATUS,
            'sort' => true,
            'classes' => 'minW80',
        ]);
    }

    private function setupFields()
    {
        $this->crud->createField([
            'name' => 'name',
            'class' => 'col-md-12',
            'label' => 'Назва',
            'required' => true,
        ]);

        $this->crud->createField([
            'name' => 'status',
            'class' => 'col-md-4',
            'label' => 'Статус',
            'type' => CrudField::INPUT_SELECT,
            'values' => [
                Product::STATUS_DRAFT => 'Чорнетка',
                Product::STATUS_ACTIVE => 'Активний',
                Product::STATUS_SOLD => 'Продано',
            ],
            'required' => true,
        ]);

        $this->crud->createField([
            'name' => 'price',
            'class' => 'col-md-4',
            'label' => 'Ціна',
            'type' => CrudField::INPUT_FLOAT,
            'required' => true,
        ]);

        $this->crud->createField([
            'name' => 'quantity',
            'class' => 'col-md-4',
            'label' => 'К-сть',
            'defaultValue' => 0,
            'type' => CrudField::INPUT_NUMBER,
            'required' => true,
        ]);


        $this->crud->createField([
            'name' => 'categories',
            'class' => 'col-md-12',
            'type' => CrudField::INPUT_SELECT2_MULTIPLE,
            'label' => trans('admin.categories'),
            'values' => array_values(Category::all()->toArray()),
            'required' => true,
        ]);

        $this->crud->createField([
            'name' => 'description',
            'class' => 'col-md-12',
            'label' => 'Опис',
            'defaultValue' => 0,
            'type' => CrudField::INPUT_EDITOR,
            'required' => true,
        ]);
    }

    private function setupFilters()
    {
        $this->crud->createFilter([
            'name' => 'id',
            'class' => 'col-md-3',
            'type' => CrudFilter::FILTER_INPUT,
            'keyReduce' => 'id',
            'placeholder' => 'Артикул',
        ], function () {
            return [];
        }, function ($value) {
            $this->crud->query = $this->crud->query->where('id', 'LIKE', '%'.$value.'%');
        });

        $this->crud->createFilter([
            'name' => 'status',
            'class' => 'col-md-3',
            'type' => CrudFilter::FILTER_SELECT2_BY_KEY,
            'keyReduce' => 'id',
            'placeholder' => 'Статус',
        ], function () {
            return [
                Product::STATUS_DRAFT => 'Чорнетка',
                Product::STATUS_ACTIVE => 'Активний',
                Product::STATUS_SOLD => 'Продано',
            ];
        }, function ($value) {
            $this->crud->query = $this->crud->query->where('status', (int)$value);
        });
    }
}
