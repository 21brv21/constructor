<?php

namespace App\Http\Requests;

use App\Models\Product;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CrudProductRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'price' => ['required', 'numeric'],
            'quantity' => ['required', 'integer'],
            'status' => ['required', Rule::in(Product::STATUSES)],
            'name' => ['required','string', 'max:65535'],
            'manufacturer_id' => ['required', 'integer', 'exists:manufacturers,id'],

            'categories' => ['required', 'array', 'min:1'],
            'categories.*' => ['exists:categories,id'],
        ];
    }
}
