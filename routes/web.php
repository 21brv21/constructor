<?php

use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;
use App\Http\Controllers\CategoryCrudController;
use App\Http\Controllers\ManufacturerCrudController;
use App\Http\Controllers\ProductCrudController;

use App\Services\Crud\CrudRoute;

Route::get('/', function () {
    return Inertia::render('Welcome', [
        'canLogin' => Route::has('login'),
        'canRegister' => Route::has('register'),
        'laravelVersion' => Application::VERSION,
        'phpVersion' => PHP_VERSION,
    ]);
});

Route::middleware(['auth:sanctum', config('jetstream.auth_session'), 'verified'])->group(function () {
    Route::get('/dashboard', function () {
        return Inertia::render('Dashboard');
    })->name('dashboard');


    CrudRoute::resource('categories', CategoryCrudController::class);
    CrudRoute::resource('manufacturers', ManufacturerCrudController::class);
    CrudRoute::resource('products', ProductCrudController::class);
});
