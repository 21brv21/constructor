//DEFAULT
import TextColumn from '../../Components/Crud/Columns/TextColumn.vue'
import BooleanColumn from '../../Components/Crud/Columns/BooleanColumn.vue'
import ColorColumn from '../../Components/Crud/Columns/ColorColumn.vue'
import MultipleColumn from '../../Components/Crud/Columns/MultipleColumn.vue'
import PhoneColumn from '../../Components/Crud/Columns/PhoneColumn.vue'
import NumberColumn from '../../Components/Crud/Columns/NumberColumn.vue'
import ImageColumn from '../../Components/Crud/Columns/ImageColumn.vue'
import CopyColumn from '../../Components/Crud/Columns/CopyColumn.vue'


//CUSTOM
import StatusColumn from '../../Components/Crud/Columns/StatusColumn.vue'

export default {
    components: {
        TextColumn,
        BooleanColumn,
        ColorColumn,
        MultipleColumn,
        PhoneColumn,
        NumberColumn,
        ImageColumn,
        CopyColumn,

        StatusColumn,
    },
}
