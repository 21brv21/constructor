export default {
    data: () => ({
        lang: null,
    }),
    methods: {
        setLang(ln) {
            this.lang = ln
        },
        getLang() {
            this.lang = document.documentElement.lang
        },
    },
    created() {
        this.getLang()
    }
}
