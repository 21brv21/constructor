import Select2Filter from '../../Components/Crud/Filters/Select2Filter.vue'
import Select2KeyFilter from '../../Components/Crud/Filters/Select2KeyFilter.vue'
import SearchApiFilter from '../../Components/Crud/Filters/SearchApiFilter.vue'
import Select2ByKeyFilter from '../../Components/Crud/Filters/Select2ByKeyFilter.vue'
import InputFilter from '../../Components/Crud/Filters/InputFilter.vue'


export default {
    components: {
        Select2Filter,
        Select2KeyFilter,
        SearchApiFilter,
        Select2ByKeyFilter,
        InputFilter,
    },
}
