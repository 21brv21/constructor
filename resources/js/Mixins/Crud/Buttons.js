import LinkButton from '../../Components/Crud/Buttons/LinkButton.vue'
import ClickButton from '../../Components/Crud/Buttons/ClickButton.vue'


export default {
    components: {
        LinkButton,
        ClickButton,
    },
}
