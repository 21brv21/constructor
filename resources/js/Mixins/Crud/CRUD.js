import message from "@/Mixins/message";

export default {
    mixins:[
        message
    ],
    data: () => ({
        showModal: false,
        disabledBtn: false,
        componentKey: 0,
        url: '',
        items:[]
    }),
    methods: {
        store(data) {
            this.LoaderIndicator(true);
            axios.post(this.url,  data)
                .then(response => {
                    this.items.push(response.data);
                    this.LoaderIndicator(false);
                    this.showMessage('success');
                    this.reset();
                }).catch(error => {
                    this.LoaderIndicator(false);
                    this.showErrors(error);
                }
            );
        },
        update(id, data) {
            this.LoaderIndicator(true);
            axios.put(this.url + '/' + id,  data)
                .then(response => {
                    this.items = this.items.map(item => {
                        return item.id === id ? response.data : item;
                    });
                    this.LoaderIndicator(false);
                    this.showMessage('success');
                    this.reset();
                }).catch(error => {
                    this.LoaderIndicator(false);
                    this.showErrors(error);
                }
            );
        },
        destroy(id) {
            if(confirm('Выдалити' + '?')) {
                this.LoaderIndicator(true);
                axios.delete(this.url + '/' + id)
                    .then(response => {
                        this.items.filter(item => item.id !== id);
                        this.LoaderIndicator(false);
                        this.showMessage('success');
                        this.reset();
                    }).catch(error => {
                        console.log(error)
                        this.LoaderIndicator(false);
                        this.showErrors(error);
                    }
                );
            }
        },
        reset(){

        }
    },
}
