import TextInput from '../../Components/Crud/Fields/TextInput.vue'
import PhoneInput from '../../Components/Crud/Fields/PhoneInput.vue'
import EditorInput from '../../Components/Crud/Fields/EditorInput.vue'
import ImageInput from '../../Components/Crud/Fields/ImageInput.vue'
import JsonInput from '../../Components/Crud/Fields/JsonInput.vue'
import Select2MultipleInput from '../../Components/Crud/Fields/Select2MultipleInput.vue'
import SlugInput from '../../Components/Crud/Fields/SlugInput.vue'
import Select2ArrayInput from '../../Components/Crud/Fields/Select2ArrayInput.vue'
import Select2Input from '../../Components/Crud/Fields/Select2Input.vue'
import DateInput from '../../Components/Crud/Fields/DateInput.vue'
import ColorInput from '../../Components/Crud/Fields/ColorInput.vue'
import BooleanInput from '../../Components/Crud/Fields/BooleanInput.vue'
import RadioInput from '../../Components/Crud/Fields/RadioInput.vue'
import RadioColorInput from '../../Components/Crud/Fields/RadioColorInput.vue'
import TextareaInput from '../../Components/Crud/Fields/TextareaInput.vue'
import NumberInput from '../../Components/Crud/Fields/NumberInput.vue'
import FileInput from '../../Components/Crud/Fields/FileInput.vue'
import HasManyInput from '../../Components/Crud/Fields/HasManyInput.vue'
import HasManyTextInput from '../../Components/Crud/Fields/HasManyTextInput.vue'
import SelectInput from '../../Components/Crud/Fields/SelectInput.vue'
import EdrpouInput from '../../Components/Crud/Fields/EdrpouInput.vue'
import SeoTextInput from '../../Components/Crud/Fields/SeoTextInput.vue'
import SeoTextareaInput from '../../Components/Crud/Fields/SeoTextareaInput.vue'
import FloatInput from '../../Components/Crud/Fields/FloatInput.vue'
import Select2ByKeyInput from '../../Components/Crud/Fields/Select2ByKeyInput.vue'


export default {
    components: {
        TextInput,
        TextareaInput,
        PhoneInput,
        EditorInput,
        ImageInput,
        JsonInput,
        Select2MultipleInput,
        SlugInput,
        Select2Input,
        Select2ArrayInput,
        ColorInput,
        DateInput,
        BooleanInput,
        RadioInput,
        RadioColorInput,
        NumberInput,
        FileInput,
        HasManyInput,
        HasManyTextInput,
        SelectInput,
        EdrpouInput,
        SeoTextInput,
        SeoTextareaInput,
        FloatInput,
        Select2ByKeyInput,
    },
}
