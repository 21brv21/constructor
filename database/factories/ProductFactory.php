<?php

namespace Database\Factories;

use App\Models\Product;
use App\Models\Manufacturer;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProductFactory extends Factory
{
    public function definition(): array
    {
        return [
            'name' => $this->faker->name,
            'description' => $this->faker->text,
            'price'  => rand(1000, 20000),
            'quantity' => rand(1, 10),
            'status' => $this->faker->randomElement(Product::STATUSES),
            'manufacturer_id' => Manufacturer::factory(),
        ];
    }
}
