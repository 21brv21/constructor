<?php

namespace Database\Seeders;

use App\Models\Category;
use App\Models\Product;
use Illuminate\Database\Seeder;

class ProductsSeeder extends Seeder
{
    public function run(): void
    {
        if (Product::count() && app()->isProduction()) {
            return;
        }

        Product::factory(100)
            ->has(Category::factory(rand(1,3)))
            ->create();
    }
}
