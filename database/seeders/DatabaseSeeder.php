<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    public function run(): void
    {
         User::factory()->create([
             'name' => 'User',
             'email' => 'user@test.net',
             'password' => Hash::make(123456),
         ]);

        $this->call(CategoriesSeeder::class);
        $this->call(ManufacturersSeeder::class);
        $this->call(ProductsSeeder::class);
    }
}
