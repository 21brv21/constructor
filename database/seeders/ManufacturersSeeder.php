<?php

namespace Database\Seeders;

use App\Models\Manufacturer;
use Illuminate\Database\Seeder;

class ManufacturersSeeder extends Seeder
{
    public function run(): void
    {
        if (Manufacturer::count() && app()->isProduction()) {
            return;
        }

        Manufacturer::factory(10)->create();
    }
}
