<?php

namespace Database\Seeders;

use App\Models\Category;
use Illuminate\Database\Seeder;

class CategoriesSeeder extends Seeder
{
    public function run(): void
    {
        if (Category::count() && app()->isProduction()) {
            return;
        }

        Category::factory(10)->create();
    }
}
